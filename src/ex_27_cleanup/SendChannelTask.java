package ex_27_cleanup;

class SendChannelTask {
    private static int SmallestDivLeft1 = 1001;
    private static int SmallestDibLeft3 = 1001;
    private static int SmallestAs3 = 1001;
    private static int SecSmallestAs3 = 1001;
    private static int N;
    private static int[] N_values;
    private static int controlValue;

    SendChannelTask(int N,int[] N_values,int controlValue){
        SendChannelTask.N = N;
        SendChannelTask.N_values = N_values;
        SendChannelTask.controlValue = controlValue;
    }

        int calcControlValue() {
            int res1, res2;
            int result;

            for (int i = 1; i < N; i++) {
                int x = N_values[i];
                if (x % 3 == 0) {
                    if (x < SmallestAs3) {
                        SmallestAs3 = x;
                    }
                    if (x < SecSmallestAs3) {
                        SecSmallestAs3 = x;
                    }
                }
                if (x % 3 == 1) {
                    SmallestDivLeft1 = Math.min(SmallestDivLeft1, x);
                }
                if (x % 3 == 2) {
                    SmallestDibLeft3 = Math.min(SmallestDibLeft3, x);
                }
            }

            if ((SmallestDivLeft1 < 1001) && (SmallestDibLeft3 < 1001)) {
                res1 = SmallestDivLeft1 + SmallestDibLeft3;
            } else res1 = 2000;

            if ((SmallestAs3 < 1001) && (SecSmallestAs3 < 1001)) {
                res2 = SmallestAs3 + SecSmallestAs3;
            } else res2 = 2000;

            result = Math.min(res1, res2);

            if (result == 2000){
                result = 1;
            }

            System.out.println("Вычисленное контрольное значение " + result);

            System.out.println("Введите контрольное значение");
            int R = controlValue;

            if (R == result){
                System.out.println("Контроль пройден");
            } else System.out.println("Контроль не пройден");
            return result;
    }
}
