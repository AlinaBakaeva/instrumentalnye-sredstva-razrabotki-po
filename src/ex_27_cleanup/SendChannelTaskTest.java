package ex_27_cleanup;

import org.junit.Assert;

class SendChannelTaskTest {
    @org.junit.jupiter.api.Test
    void main(){
        int actually = 27;
        SendChannelTask sct = new SendChannelTask(6, new int[]{100,8,33,145,19,84},actually);

        int expect = sct.calcControlValue();

        Assert.assertEquals(expect,actually);
    }
}