package ex_27_logging;

import java.io.FileInputStream;
import java.util.logging.LogManager;
import java.util.logging.Logger;

class LoggerCreating {
    static Logger getLogger(){
        Logger logger = null;
        try(FileInputStream configFile = new FileInputStream("./src/ex_27_logging/loggerConfigurations/logger.config")){
            LogManager.getLogManager().readConfiguration(configFile);
            logger = Logger.getLogger(SendChannelTask.class.getName());
        }catch (Exception ignore){
            ignore.printStackTrace();
        }
        return logger;
    }
}
