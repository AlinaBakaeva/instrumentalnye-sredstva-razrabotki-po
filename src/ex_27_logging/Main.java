package ex_27_logging;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {
    private static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args){
        try {
            System.out.println("Введите значение N: ");
            int N = scanner.nextInt();
            int[] N_values = new int[N];

            for (int i = 0; i < N_values.length; i++) {
                System.out.println("Введите значение x" + i);
                int x = scanner.nextInt();
                N_values[i] = x;
            }

            System.out.println("Введите контрольное значение R: ");
            int R = scanner.nextInt();

            SendChannelTask sendChannelTask = new SendChannelTask(N, N_values, R);
            sendChannelTask.calcControlValue();
        }catch (InputMismatchException e){
            System.out.println("Произошло исключение: введено не-числовое значение");
        }
    }
}
