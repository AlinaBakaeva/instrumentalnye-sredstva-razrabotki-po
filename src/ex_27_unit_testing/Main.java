package ex_27_unit_testing;

import java.util.Scanner;

public class Main {
    private static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args){
        System.out.println("Введите значение N: ");
        int N = scanner.nextInt();
        int[] N_values = new int[N];

        for (int i=0;i<N_values.length;i++){
            System.out.println("Введите значение x"+i);
            int x = scanner.nextInt();
            N_values[i] = x;
        }

        System.out.println("Введите контрольное значение R: ");
        int R = scanner.nextInt();

        SendChannelTask sendChannelTask = new SendChannelTask(N,N_values,R);
        sendChannelTask.calcControlValue();

    }
}
